#! /bin/sh

set -e

tests="minc_types.c icv_range.c icv.c icv_dim.c icv_dim1.c icv_fillvalue.c"

for test in $tests;
do
   echo Testing $test
   ./$test > junk.out
   diff $srcdir/$test.out junk.out && rm junk.out

done

